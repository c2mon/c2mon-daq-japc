/******************************************************************************
 * Copyright (C) 2010-2021 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.daq.japc.wie;

import cern.japc.core.AcquiredParameterValue;
import cern.japc.value.*;
import lombok.extern.slf4j.Slf4j;

import cern.c2mon.daq.japc.GenericJapcMessageHandler;
import cern.c2mon.shared.common.datatag.ISourceDataTag;
import cern.c2mon.shared.common.datatag.SourceDataTagQuality;
import cern.c2mon.shared.common.datatag.address.JAPCHardwareAddress;
import cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode;

/**
 * A dedicated JAPC handler for Wiener Fan Traydevices
 */
@Slf4j
public class WieJapcMessageHandler extends GenericJapcMessageHandler {

  public static final String DEFAULT_HASHNAMES_FIELD = "names";

  @Override
  protected void handleJAPCValue(ISourceDataTag tag, String pParameterName, AcquiredParameterValue pParameterValue) {
    ParameterValue value = pParameterValue.getValue();
    long timeStamp = System.currentTimeMillis();
    Type type = value.getType();
    JAPCHardwareAddress addr = (JAPCHardwareAddress) tag.getHardwareAddress();

    log.debug("Update received for parameter: {} and value of type {}", pParameterName, value.getType().toString());

    if (type != Type.MAP) {
      String errorMsg = String.format("handleJAPCValue() : Type \"%s\" is not supported", type.toString());
      log.error("\t{}", errorMsg);
      getEquipmentMessageSender().update(tag.getId(),
          new SourceDataTagQuality(SourceDataTagQualityCode.INCORRECT_NATIVE_ADDRESS, errorMsg));
      return;
    }

    MapParameterValue mapValue = (MapParameterValue) value;

    try {
      log.debug("DataFieldName is {}", addr.getDataFieldName());
      SimpleParameterValue svalue = mapValue.get(addr.getDataFieldName());
      log.debug("{} is the type of {}", svalue.getType().toString(), addr.getDataFieldName());
      ValueType valueType = svalue.getValueType();

      if (valueType.isScalar()) {
        // Simple scalar field
        svalue = mapValue.get(addr.getDataFieldName());
        sendJAPCSValueFromScalar(tag, svalue, null, convertSourceTimestampToMs(timeStamp));
      } else if (valueType.isArray()) {
        int index = addr.getColumnIndex();
        if (addr.getIndexFieldName() != null) {
          if (mapValue.get(addr.getDataFieldName() + "." + DEFAULT_HASHNAMES_FIELD) == null) {
            throw new ArrayIndexOutOfBoundsException(addr.getDataFieldName() + "."
                    + DEFAULT_HASHNAMES_FIELD + " is missing");
          }

          index = getIndex(mapValue, addr.getDataFieldName() + "." + DEFAULT_HASHNAMES_FIELD,
                  addr.getIndexFieldName());
        }
        sendJAPCSValueFromArray(tag, svalue, valueType, timeStamp, index);
      }
    }
    catch (Exception e) {
      log.warn("\tInvalidating SourceDataTagValue with quality INCORRECT_NATIVE_ADDRESS, for Tag name {} (#{}). Problem: {}",
          tag.getName(), tag.getId(), e.getMessage());
      getEquipmentMessageSender().update(tag.getId(),
          new SourceDataTagQuality(SourceDataTagQualityCode.INCORRECT_NATIVE_ADDRESS, e.getMessage()));
    }
  }

  @Override
  protected boolean isSelectorOnChangeEnabled() {
    return false;
  }

  @Override
  public void refreshAllDataTags() {
    // TODO Implement this method at the moment it might be part of the connectToDataSourceMehtod
  }

  @Override
  public void refreshDataTag(long dataTagId) {
    // TODO Implement this method.
  }

}
