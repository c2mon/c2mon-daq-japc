/*******************************************************************************
 * Copyright (C) 2010-2020 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cern.c2mon.daq.japc;

import java.util.concurrent.*;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import cern.c2mon.shared.common.datatag.ISourceDataTag;

/**
 * This class implements a mechanism of periodic re-subscription for tags that failed to subscribe correctly at
 * start-up
 */
@Slf4j
final class TagConnectionMonitor {
  
  /** in seconds */
  static final Integer MIN_RECONNECTION_TIME = 60;
  /** in seconds */
  private static final Integer MAX_RECONNECTION_TIME = 1800;
  /** in seconds */
  private static final Integer RECONNECTION_TIME_STEP = 60;

  private static final Integer RECONNECTION_THREAD_POOL_SIZE = 32;

  @Getter
  private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(RECONNECTION_THREAD_POOL_SIZE);
  @Getter
  private final ConcurrentMap<Long, Future<?>> futures = new ConcurrentHashMap<>();
  @Getter
  private final ConcurrentMap<Long, Integer> reconnectInMap = new ConcurrentHashMap<>();
  
  private final ITagRegister tagRegister;
  
  TagConnectionMonitor(ITagRegister tagRegister) {
    this.tagRegister = tagRegister;
  }

  void add(ISourceDataTag tag) {
    try {
      if (!reconnectInMap.containsKey(tag.getId())) {
        reconnectInMap.put(tag.getId(), MIN_RECONNECTION_TIME);
      }

      if (log.isDebugEnabled()) {
            int reconnectIn = reconnectInMap.get(tag.getId());
            if ((reconnectIn / 60) > 1.0f) {
              log.debug("scheduling re-connection task for tag {}, name: {} The task will run in {} min {} sec.",
                              tag.getId(), tag.getName(), reconnectIn / 60, reconnectIn % 60);
            }
            else {
              log.debug("scheduling re-connection task for tag {}, name: {} The task will run in {} sec.", tag.getId(), tag.getName(), reconnectIn);
            }
      }
    
      if (futures.get(tag.getId()) != null) {
            Future<?> f = futures.get(tag.getId());
            if (!f.isDone()) {
              f.cancel(true);
            }
    
            futures.remove(tag.getId());
      }
    
      log.info("Current futures size: {}", futures.size());
    
      futures.put(tag.getId(),
                  executor.schedule(new TagReconnectionTask(tag), reconnectInMap.get(tag.getId()), TimeUnit.SECONDS));
    } catch (Exception e) {
      log.error("Failed to add tag #{} to TagConnectionMonitor for later subscription", tag.getId(), e);
    }
  }

  public void remove(ISourceDataTag tag) {
    if (futures.containsKey(tag.getId())) {
      Future<?> f = futures.get(tag.getId());
      log.debug("cancelling re-connection task for tag {}", tag.getId());
      f.cancel(true);
    }
  }

  public synchronized void stop() {
    this.executor.shutdownNow();
  }


  class TagReconnectionTask implements Runnable {

    private ISourceDataTag tag = null;

    public TagReconnectionTask(ISourceDataTag tag) {
      this.tag = tag;
    }

    @Override
    public void run() {

      try {
        log.debug("calling registerTag({})", tag.getId());
        tagRegister.registerTag(tag);

        // we're connected fine, reset the reconnection time
        reconnectInMap.put(tag.getId(), MIN_RECONNECTION_TIME);
      }
      catch (TagOperationException ex) {
        log.debug("registerTag({}) failed", tag.getId());

        int reconnectIn = reconnectInMap.get(tag.getId());
        if (reconnectIn < MAX_RECONNECTION_TIME) {
          reconnectIn += RECONNECTION_TIME_STEP;
          reconnectInMap.put(tag.getId(), reconnectIn);
        }

        // re-schedule reconnection task for this tag
        add(tag);
      }
      catch (Exception e) {
        log.warn(e.toString());
      }
    }// run
  }
}