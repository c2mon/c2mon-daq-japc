/******************************************************************************
 * Copyright (C) 2010-2021 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.daq.japc;

import static cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode.CONVERSION_ERROR;
import static cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode.DATA_UNAVAILABLE;
import static cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode.INCORRECT_NATIVE_ADDRESS;
import static cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode.UNKNOWN;
import static java.lang.String.format;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import cern.japc.core.*;
import cern.japc.core.factory.MapParameterValueFactory;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.factory.SimpleParameterValueFactory;
import cern.japc.value.*;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.authentication.LoginPolicy;
import cern.rbac.util.authentication.LoginService;
import cern.rbac.util.authentication.LoginServiceBuilder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import cern.c2mon.daq.common.EquipmentMessageHandler;
import cern.c2mon.daq.common.ICommandRunner;
import cern.c2mon.daq.common.conf.equipment.ICommandTagChanger;
import cern.c2mon.daq.common.conf.equipment.IDataTagChanger;
import cern.c2mon.daq.tools.equipmentexceptions.EqCommandTagException;
import cern.c2mon.daq.tools.equipmentexceptions.EqIOException;
import cern.c2mon.shared.common.command.ISourceCommandTag;
import cern.c2mon.shared.common.datatag.ISourceDataTag;
import cern.c2mon.shared.common.datatag.SourceDataTagQuality;
import cern.c2mon.shared.common.datatag.ValueUpdate;
import cern.c2mon.shared.common.datatag.address.JAPCHardwareAddress;
import cern.c2mon.shared.daq.command.SourceCommandTagValue;
import cern.c2mon.shared.daq.config.ChangeReport;
import cern.c2mon.shared.daq.config.ChangeReport.CHANGE_STATE;

/**
 * This is a specialized subclass of the general C2MON EquipmentMessageHandler. The class implements a generic
 * specialization for JAPC protocol.
 */
@Slf4j
public class GenericJapcMessageHandler extends EquipmentMessageHandler implements ICommandRunner, IDataTagChanger,
        ICommandTagChanger, ITagRegister, Runnable {

  public static final String DEFAULT_MAP_DATA_FIELD_NAME = "value";
  public static final String DEFAULT_TIMESTAMP_FIELD = "ts";
  public static final String DEFAULT_DETAILS_FIELD = "details";

  protected static final int MIN_RECONNECTION_TIME = 1 * 60; // 5 * 60; // in seconds
  protected static final int MAX_RECONNECTION_TIME = 30 * 60; // in seconds
  protected static final int RECONNECTION_TIME_STEP = 1 * 60; // 5 * 60; // in seconds;

  protected static final int RECONNECTION_THREAD_POOL_SIZE = 32;

  /**
   * JAPC parameter factory instance.
   */
  protected ParameterFactory parameterFactory;

  /**
   * a map keeping subscription handles
   */
  private final Map<Long, SubscriptionHandle> handles = new ConcurrentHashMap<>();

  /**
   * a map keeping for each tag, its ParameterValueListener
   */
  private final Map<Long, ParameterValueListener> pvlistenersMap = new ConcurrentHashMap<>();

  private TagConnectionMonitor tagConnectionMonitor;

  // authentication by location is enabled
  protected static String user = "not-used";
  protected static String pass = "not-used";
  protected static String rbacAppName = "JAPC-RDA-DAQ";

  @Setter
  private static LoginService loginService;

  protected static final void initRbac() throws EqIOException {
    if (null == loginService) {
      try {
        loginService = LoginServiceBuilder.newInstance()
        .userName(user)
        .userPassword(pass)
        .loginPolicy(LoginPolicy.LOCATION)
        .applicationName(rbacAppName)
        .autoRefresh(true)
        .build();
      } catch (AuthenticationException ex) {
        loginService = null;
        throw new EqIOException("RBAC initialization failed! " + ex.getMessage());
      }
    }
  }
  
  protected GenericJapcMessageHandler(boolean createTagConnectionMonitor) {
    if (createTagConnectionMonitor && tagConnectionMonitor == null) {
      tagConnectionMonitor = new TagConnectionMonitor(this);
    }
  }

  public GenericJapcMessageHandler() {
    if (tagConnectionMonitor == null) {
      tagConnectionMonitor = new TagConnectionMonitor(this);
    }
  }

  class JapcHandlerValueListener implements ParameterValueListener {

    private final ISourceDataTag tag;

    public JapcHandlerValueListener(final ISourceDataTag tag) {
      this.tag = tag;
    }

    @Override
    public void exceptionOccured(String parameterId, String description, ParameterException e) {
      log.debug("exceptionOccured() : ParameterException caught: " + e.getClass() + " with message " + e.getMessage());

      if (e instanceof SubscriptionRecoveredException) {
        // don't do anything, just log it - we need to wait for the new values to come anyway
        // don't do anything here, it should be the japc extension job to resent the latest value after reconnection
        log.debug("SubscriptionRecoveredException caught, connection's back", e);
      } else if (e instanceof NoDataAvailableException) {
        // Exception indicating no available data at the data source (could be considered as normal operational situation).
        log.debug("NoDataAvailableException caught for tag #{}, which will be ignored: {}", tag.getId(), description);
      } else {
        // invalidate the tag
        handleJAPCException(tag, description);
        if (e instanceof SubscriptionProblemException) {
          log.debug("SubscriptionProblemException caught, connection's lost", e);
        }
      }
    }

    @Override
    public void valueReceived(String parameterId, AcquiredParameterValue parameterValue) {
      handleJAPCValue(tag, parameterId, parameterValue);
    }
  }

  /**
   * this method can be overridden by inheriting classes, if needed Default implementation is empty
   *
   * @throws EqIOException
   */
  protected void beforeConnectToDataSource() throws EqIOException {
    // no default implementation
  }

  @Override
  public void connectToDataSource() throws EqIOException {
    log.debug("entering connectToDataSource()..");

    this.beforeConnectToDataSource();

    // register handler as command runner
    getEquipmentCommandHandler().setCommandRunner(this);
    // register handler as data tag changer
    getEquipmentConfigurationHandler().setDataTagChanger(this);

    // register handler as command tag changer
    getEquipmentConfigurationHandler().setCommandTagChanger(this);

    // If this is the first time this method is called (on start-up), create
    // a JAPC parameter factory
    if (this.parameterFactory == null) {
      try {
        this.parameterFactory = ParameterFactory.newInstance();
        // We do not really have an Equipment but it indicates at least that
        // the factory creates went fine.
        getEquipmentMessageSender().confirmEquipmentStateOK();
      }
      catch (Exception e) {
        getEquipmentMessageSender().confirmEquipmentStateIncorrect(
                "Unexpected problem occured when trying to create a JAPC ParameterFactory instance");

        log
                .error("connectToDataSource() : Unexpected problem occured when trying to create a JAPC ParameterFactory",
                        e);
        throw new EqIOException("Unexpected problem occured while creating instance of ParameterFactory: "
                + e.getMessage());
      }
    }

    new Thread(this).start();

    log.debug("leaving connectToDataSource()");
  }

  @Override
  public void run() {
    for (ISourceDataTag tag : getEquipmentConfiguration().getSourceDataTags().values()) {
      try {
        registerTag(tag);
      }
      catch (Exception ex) {
        log.error(ex.getMessage());
        getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(DATA_UNAVAILABLE, ex.getMessage()));

        tagConnectionMonitor.add(tag);
      }
    }
  }// run

  /**
   * Registers new tag. starts subscription etc..
   *
   * @param tag
   */
  @Override
  public void registerTag(ISourceDataTag tag) throws TagOperationException {
    log.trace("entering registerTag({})", tag.getId());

    // check if there's any listener registered already for that tag
    ParameterValueListener regListener = this.pvlistenersMap.get(tag.getId());

    // check if there's handle registered for that tag
    SubscriptionHandle regHandle = this.handles.get(tag.getId());

    // none of the above should be present
    if (regListener != null || regHandle != null) {
      log.warn("tag: {} is already registered. You must unregister it first!", tag.getId());
      return;
    }

    SubscriptionHandle handle = null;
    try {
      JAPCHardwareAddress addr = (JAPCHardwareAddress) tag.getHardwareAddress();

      Parameter parameter = this.parameterFactory.newParameter(addr.getDeviceName(), addr.getPropertyName());

      Selector selector = getJapcSelector(tag);

      log.debug("creating subscription handle for parameter: {}  selector: {}}", parameter.getName(), selector.toString());

      ParameterValueListener pvl = new JapcHandlerValueListener(tag/* , parameter */);

      handle = parameter.createSubscription(selector, pvl);

      handle.startMonitoring();

      log.debug("successfully subscribed to parameter: {}", parameter.getName());

      this.handles.put(tag.getId(), handle);
      this.pvlistenersMap.put(tag.getId(), pvl);

    }
    catch (Exception ex) {

      String err = format("Problem desc: %s", ex.getMessage());
      log.error(ex.getMessage(), ex);

      throw new TagOperationException(tag.getId(), err);
    } finally {
      log.trace("leaving registerTag({})", tag.getId());
    }
  }

  /**
   * @param tag
   *
   * @throws TagOperationException
   */
  protected void unregisterTag(ISourceDataTag tag) throws TagOperationException {
    log.trace("entering unregisterTag({})", tag.getId());

    // check if there's handle registered for that tag
    SubscriptionHandle regHandle = this.handles.get(tag.getId());

    try {
      if (regHandle != null) {
        regHandle.stopMonitoring();
        tagConnectionMonitor.remove(tag);
      }
      else {
        log.warn("reg handle for tag: {} was null", tag.getId());
      }
      this.handles.remove(tag.getId());
      this.pvlistenersMap.remove(tag.getId());

    }
    catch (Exception ex) {
      String err = format("Unable to stop monitoring for tag: %d. Problem description: %s", tag.getId(),
              ex.getMessage());
      log.error(err, ex);
      throw new TagOperationException(err);
    } finally {
      log.trace("leaving unregisterTag({})", tag.getId());
    }
  }

  @Override
  public void disconnectFromDataSource() throws EqIOException {
    log.debug("entering diconnectFromDataSource()..");

    for (ISourceDataTag tag : getEquipmentConfiguration().getSourceDataTags().values()) {
      try {
        unregisterTag(tag);
      }
      catch (TagOperationException ex) {
        log.warn(ex.getMessage());
      }
    }// for

    this.handles.clear();
    this.pvlistenersMap.clear();

    log.debug("leaving diconnectFromDataSource()");
  }

  /**
   * this is the default implementation of the handleJAPCValue method. For more specific JAPC handlers this method
   * should be overwritten
   *
   * @param tag
   * @param pParameterName
   * @param pParameterValue
   */
  protected void handleJAPCValue(final ISourceDataTag tag, final String pParameterName,
                                 final AcquiredParameterValue pParameterValue) {
    ParameterValue value = pParameterValue.getValue();
    Type type = value.getType();

    // ValueHeader header = pParameterValue.getHeader();
    JAPCHardwareAddress addr = (JAPCHardwareAddress) tag.getHardwareAddress();

    log.debug("handleJAPCValue(): update received for parameter: {}", pParameterName);
    log.debug("handleJAPCValue(): value of type: {} received", value.getType().toString());

    if (type == Type.SIMPLE) {

      log.debug("\tupdate type : SIMPLE");

      SimpleParameterValue simpleValue = (SimpleParameterValue) value;

      log.debug("\tthe value-type: {}", simpleValue.getValueType().toString());

      ValueType valueType = simpleValue.getValueType();

      if (valueType.isScalar()) {
        try {
          sendJAPCSValueFromScalar(tag, simpleValue, "", System.currentTimeMillis());
        }
        catch (Exception ex) {
          log.error("handleJAPCValue(): {}", ex.getMessage(), ex);
          getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(UNKNOWN, ex.getMessage()));
        }
      }

    }
    else if (type == Type.MAP) {
      MapParameterValue mapValue = (MapParameterValue) value;

      try {
        String dataFieldName = getValueOrDefault(addr.getDataFieldName(), DEFAULT_MAP_DATA_FIELD_NAME);
        String timestampFieldName = format("%s.%s", dataFieldName, DEFAULT_TIMESTAMP_FIELD);
        String detailsFieldName = format("%s.%s", dataFieldName, DEFAULT_DETAILS_FIELD);

        // get the simple value for the map

        if (mapValue.size() > 0) {

          SimpleParameterValue svalue = mapValue.get(dataFieldName);
          SimpleParameterValue timestampValue = mapValue.get(timestampFieldName);
          SimpleParameterValue detailsValue = mapValue.get(detailsFieldName);

          if (svalue == null) {
            String errMessage = String.format(
                    "Field [%s] missing in the map. Please check your configuration.",
                    dataFieldName);
            throw new IndexOutOfBoundsException(errMessage);
          }

          // if timestamp field is provided - take the timestamp value from it, otherwise - take the
          // system's time
          long timestamp = timestampValue == null ? System.currentTimeMillis() : timestampValue.getLong();

          if (detailsValue != null) {
            sendJAPCSValueFromScalar(tag, svalue, detailsValue.getString(),
                    convertSourceTimestampToMs(timestamp));
          }
          else {
            sendJAPCSValueFromScalar(tag, svalue, null, convertSourceTimestampToMs(timestamp));
          }

        }// if mapValue.size() > 0
        else {
          log.info("received an empty map for parameter: {} (missing initial update?)", pParameterName);
        }
      }
      catch (Exception e) {
        log.warn("\tInvalidating SourceDataTagValue with quality INCORRECT_NATIVE_ADDRESS, for Tag name : {}, id : {},  problem: {}", tag.getName(), tag.getId(), e.getMessage());
        getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(INCORRECT_NATIVE_ADDRESS, e.getMessage()));
      }

    }
    else {
      String errorMsg = String.format("Type \"%s\" is not supported", type.toString());
      log.error("\t" + errorMsg);
      getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(INCORRECT_NATIVE_ADDRESS, errorMsg));
      return;
    }

  }

  /**
   * // TODO: this method should return true, temporarily returns false because of the bug in the japc-mockito
   * (otherwise the tests would fail)
   *
   * @return
   */
  protected boolean isSelectorOnChangeEnabled() {
    return false;
  }

  protected final void handleJAPCException(final ISourceDataTag tag, final String pDescription) {
    getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(DATA_UNAVAILABLE, pDescription));
  }

  protected final void sendJAPCSValueFromScalar(final ISourceDataTag tag, final SimpleParameterValue sValue,
                                                final String valueDescription, final long sourceTimestamp) {
    log.debug("enetring sendJAPCSValueFromScalar()..");

    Object value4send = null;
    ValueType valueT = sValue.getValueType();

    // do not convert - conversion is now done by the daq core!
    if (valueT == ValueType.BOOLEAN) {
      value4send = sValue.getBoolean();
    }
    else if (valueT == ValueType.BYTE) {
      value4send = sValue.getByte();
    }
    else if (valueT == ValueType.INT) {
      value4send = sValue.getInt();
    }
    else if (valueT == ValueType.LONG) {
      value4send = sValue.getLong();
    }
    else if (valueT == ValueType.FLOAT) {
      value4send = sValue.getFloat();
    }
    else if (valueT == ValueType.DOUBLE) {
      value4send = sValue.getDouble();
    }
    else if (valueT == ValueType.STRING) {
      value4send = sValue.getString();
    }

    if (value4send != null) {
      // send the value to the server
      getEquipmentMessageSender().update(tag.getId(), new ValueUpdate(value4send, valueDescription, sourceTimestamp));
    }
    else {
      log.info(
              "\tInvalidating SourceDataTagValue with quality CONVERSION_ERROR, for Tag name : " + tag.getName()
                      + " id : " + tag.getId());
      getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(CONVERSION_ERROR));
    }

    log.debug("leaving sendJAPCSValueFromScalar()");
  }

  protected final void sendJAPCSValueFromArray(final ISourceDataTag tag, final SimpleParameterValue simpleValue,
                                               final ValueType valueType, final ValueHeader header, final int index) {
    this.sendJAPCSValueFromArray(tag, simpleValue, valueType,
            convertSourceTimestampToMs(header.getAcqStampMillis()), index);
  }

  protected final void sendJAPCSValueFromArray(final ISourceDataTag tag, final SimpleParameterValue simpleValue,
                                               final ValueType valueType, final long timestamp, final int index) {
    log.trace("enetring sendJAPCSValueFromArray()..");

    Object value4send = null;

    try {

      // we don't convert, it is now done by the DAQ core
      if (valueType == ValueType.BOOLEAN_ARRAY) {
        value4send = simpleValue.getBoolean(index);
      }
      else if (valueType == ValueType.BYTE_ARRAY) {
        value4send = simpleValue.getByte(index);
      }
      else if (valueType == ValueType.INT_ARRAY) {
        value4send = simpleValue.getInt(index);
      }
      else if (valueType == ValueType.LONG_ARRAY) {
        value4send = simpleValue.getLong(index);
      }
      else if (valueType == ValueType.FLOAT_ARRAY) {
        value4send = simpleValue.getFloat(index);
      }
      else if (valueType == ValueType.DOUBLE_ARRAY) {
        value4send = simpleValue.getDouble(index);
      }
      else if (valueType == ValueType.STRING_ARRAY) {
        value4send = simpleValue.getString(index);
      }

      if (value4send != null) {
        // send the value to the server
        getEquipmentMessageSender().update(tag.getId(), new ValueUpdate(value4send, timestamp));
      }
      else {
        log.info(
                "\tInvalidating SourceDataTagValue with quality CONVERSION_ERROR, for Tag name : "
                        + tag.getName() + " id : " + tag.getId());
        getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(CONVERSION_ERROR));
      }

    }
    catch (java.lang.ArrayIndexOutOfBoundsException ex) {
      log.warn("could not read data from an array at index : " + index);
      log.info(
              "\tInvalidating SourceDataTagValue with quality INCORRECT_NATIVE_ADDRESS, for Tag name : "
                      + tag.getName() + " id : " + tag.getId());
      getEquipmentMessageSender().update(tag.getId(), new SourceDataTagQuality(INCORRECT_NATIVE_ADDRESS,
              "Could not read data from array at index : " + index));
    }

    log.trace("leaving sendJAPCSValueFromArray()");
  }

  protected final String sendCommand(SourceCommandTagValue p0) throws EqCommandTagException {
    log.trace("entering sendCommand()..");

    String result = null;

    ISourceCommandTag sct = getEquipmentConfiguration().getSourceCommandTags().get(p0.getId());

    if (sct == null) {
      throw new EqCommandTagException(String.format(
              "command #%d is not registered. Please check DAQ configuration", p0.getId()));
    }

    JAPCHardwareAddress addr = (JAPCHardwareAddress) sct.getHardwareAddress();

    StringBuilder paramUrl = new StringBuilder();
    paramUrl.append(addr.getDeviceName()).append("/").append(addr.getPropertyName());

    String dataField = addr.getDataFieldName();
    if (dataField != null && dataField.length() > 0) {
      paramUrl.append("#").append(dataField);
    }

    Selector selector = null;

    switch (addr.getCommandType()) {

      case SET:

        log.debug("executing SET command..");
        try {
          Parameter parameter = ParameterFactory.newInstance().newParameter(paramUrl.toString());

          // Create a selector for the parameter
          if (addr.getCycleSelector() != null) {
            selector = SelectorFactory.newSelector(addr.getCycleSelector());
          }
          ParameterValue pv = SimpleParameterValueFactory.newSimpleParameterValue(p0.getValue());

          log.trace("before parameter.setValue(selector, pv)");
          parameter.setValue(selector, pv);
          log.trace("after parameter.setValue(selector, pv)");
        }
        catch (Exception e) {
          throw new EqCommandTagException("command execution failed. could not set value: " + p0.getValue()
                  + " for parameter: " + paramUrl.toString() + " Error: " + e.getMessage());
        }

        break;

      case GET:

        log.debug("executing GET command..");
        try {

          log.trace("before ParameterFactory.newInstance().newParameter()");
          Parameter parameter = ParameterFactory.newInstance().newParameter(paramUrl.toString());
          log.trace("after ParameterFactory.newInstance().newParameter()");

          AcquiredParameterValue apv = null;

          // if context-field is defined
          if (addr.hasContextField()) {

            // split the fields passed inside the context
            // Note: the separator is ; (semicolon), but there may be escape characters present: \;
            String[] rawfields = addr.getContextField().trim().split("(?<!\\\\);");
            List<String> fl = new ArrayList<>();
            for (String s : rawfields) {
              fl.add(s.replace("\\;", ";"));
            }

            String[] fields = fl.toArray(new String[0]);

            // split the value passed as an argument of the command
            // Note: the separator of each values is ; (semicolon), but there may be escape characters present:
            // \;
            String[] rawvalues = p0.getValue().toString().trim().split("(?<!\\\\);");
            List<String> fv = new ArrayList<>();
            for (String s : rawvalues) {
              fv.add(s.replace("\\;", ";"));
            }

            String[] values = fv.toArray(new String[0]);

            // make sure that number of fields matches number of values
            if (fields.length != values.length)
              throw new EqCommandTagException(
                      "number of fields in the context does not match number of values for that context");

            SimpleParameterValue[] spvArray = new SimpleParameterValue[fields.length];

            for (int i = 0; i < fields.length; i++) {

              // check if a value is an array or not
              String v = values[i].trim();

              // if value is an array of strings ( this should be indicated by curly brackets )
              if (v.startsWith("{") && v.endsWith("}")) {
                String[] valsArray = v.substring(1, v.length() - 1).split(",");
                spvArray[i] = SimpleParameterValueFactory.newSimpleParameterValue(valsArray);
              }
              else {
                spvArray[i] = SimpleParameterValueFactory.newSimpleParameterValue(values[i]);
              }
            }

            log.trace("before creating selector");
            selector = SelectorFactory.newSelector(MapParameterValueFactory.newMapParameterValue(fields, spvArray));
            log.trace("after creating selector");

            log.trace("before parameter.getValue(selector)");
            apv = parameter.getValue(selector);
            log.trace("after parameter.getValue(selector)");

          }
          else { // no context-field
            selector = Selectors.NO_SELECTOR;
            apv = parameter.getValue(selector);
          }

          result = apv.getValue().getString();

        }
        catch (Exception e) {
          throw new EqCommandTagException("command execution failed. could not get value from from parameter: "
                  + paramUrl.toString() + " Error: " + e.getMessage());
        }
        break;

      default:

        throw new EqCommandTagException(String.format(
                "command #%d has unknown type. Only SET and GET commands are supported", p0.getId()));

    }// switch

    log.trace("leaving sendCommand()");
    return result;
  }

  /**
   * this method is a temporary <<hack>> to solve the problem with JAPC source timestamps delivered often in microsec.
   * istead of ns.
   *
   * @param sTimeStamp
   *
   * @return
   */
  public static final long convertSourceTimestampToMs(long sTimeStamp) {

    Calendar calendar = Calendar.getInstance();
    calendar.set(1990, 01, 01);

    Date sourceDate = new Date(sTimeStamp);

    // make sure the provided timestamp is not older than 1990-01-01
    if (sourceDate.before(calendar.getTime())) {
      return sTimeStamp * 1000;
    }
    else
      return sTimeStamp;
  }

  /**
   * This method is used to retrieve the array index for the requested value
   *
   * @param name the parameter Name
   *
   * @return the array index of the parameter name
   */
  protected final int getIndex(final MapParameterValue mpv, final String parameterName, final String fieldName)
          throws ArrayIndexOutOfBoundsException {
    String[] names = null;
    try {
      names = mpv.getStrings(parameterName);
    }
    catch (Exception ex) {
      throw new ArrayIndexOutOfBoundsException("field: " + parameterName + " not found");
    }

    if (names != null) {
      for (int i = 0; i < names.length; i++) {
        if (names[i].equals(fieldName)) {
          return i;
        }
      }
    }

    return -1;
  }

  @Override
  public final String runCommand(SourceCommandTagValue sourceCommandTagValue) throws EqCommandTagException {
    return sendCommand(sourceCommandTagValue);
  }

  public final String runCommand(Long commandId) throws EqCommandTagException {
    ISourceCommandTag command = getEquipmentConfiguration().getSourceCommandTag(commandId);
    if (command == null) {
      return format("command %d is unknown", commandId);
    }

    SourceCommandTagValue ctv = new SourceCommandTagValue(commandId, command.getName(), this
            .getEquipmentConfiguration().getId(), (short) 0, "dummy", "java.lang.String");

    return sendCommand(ctv);
  }

  /**
   * this method is called when a new DataTag is "injected"
   */
  @Override
  public final void onAddDataTag(ISourceDataTag sourceDataTag, ChangeReport changeReport) {
    log.trace("entering onAddDataTag({})..", sourceDataTag.getId());

    changeReport.setState(CHANGE_STATE.SUCCESS);

    // register tag
    try {
      registerTag(sourceDataTag);
    }
    catch (TagOperationException ex) {
      // if a problem appears when one wants to add configuration
      changeReport.appendWarn(ex.getMessage());
    }

    log.trace("leaving onAddDataTag({})", sourceDataTag.getId());
  }

  /**
   * this method is called when a request to remove a DataTag is received
   */
  @Override
  public final void onRemoveDataTag(ISourceDataTag sourceDataTag, ChangeReport changeReport) {
    log.trace("entering onRemoveDataTag({})..", sourceDataTag.getId());

    changeReport.setState(CHANGE_STATE.SUCCESS);

    // unregister tag
    try {
      unregisterTag(sourceDataTag);
    }
    catch (TagOperationException ex) {
      changeReport.appendWarn(ex.getMessage());
    }

    log.trace("leaving onRemoveDataTag({})", sourceDataTag.getId());
  }

  @Override
  public final void onUpdateDataTag(ISourceDataTag sourceDataTag, ISourceDataTag oldSourceDataTag,
                                    ChangeReport changeReport) {
    log.trace("entering onUpdateDataTag({}, {})..", sourceDataTag.getId(), oldSourceDataTag.getId());

    changeReport.setState(CHANGE_STATE.SUCCESS);

    if (!oldSourceDataTag.getHardwareAddress().equals(sourceDataTag.getHardwareAddress())) {
      try {
        log.debug("calling  unregisterTag({})..", oldSourceDataTag.getId());
        unregisterTag(oldSourceDataTag);
      }
      catch (TagOperationException ex) {
        changeReport.appendWarn(ex.getMessage());
      }
      try {
        log.debug("calling  registerTag({})..", sourceDataTag.getId());
        registerTag(sourceDataTag);
      }
      catch (TagOperationException ex) {
        changeReport.setState(CHANGE_STATE.FAIL);
        changeReport.appendError(ex.getMessage());
      }
    }// if
    else {
      changeReport.appendInfo("No change detected in the tag hardware address. No action effected");
    }

    log.trace("leaving onUpdateDataTag({}, {})", sourceDataTag.getId(), oldSourceDataTag.getId());
  }

  @Override
  public final void onAddCommandTag(ISourceCommandTag sourceCommandTag, ChangeReport changeReport) {
    log.trace("entering onAddCommandTag({})..", sourceCommandTag.getId());

    // nothing more to be done here
    changeReport.setState(CHANGE_STATE.SUCCESS);

    log.trace("leaving onAddCommandTag({})", sourceCommandTag.getId());
  }

  @Override
  public final void onRemoveCommandTag(ISourceCommandTag sourceCommandTag, ChangeReport changeReport) {
    log.trace("entering onRemoveCommandTag({})..", sourceCommandTag.getId());

    // nothing more to be done here
    changeReport.setState(CHANGE_STATE.SUCCESS);

    log.trace("leaving onRemoveCommandTag({})", sourceCommandTag.getId());
  }

  @Override
  public final void onUpdateCommandTag(ISourceCommandTag sourceCommandTag, ISourceCommandTag oldSourceCommandTag,
                                       ChangeReport changeReport) {

    log.trace("entering onUpdateCommandTag({}, {})..", sourceCommandTag.getId(), oldSourceCommandTag.getId());
    // nothing more to be done here
    changeReport.setState(CHANGE_STATE.SUCCESS);

    log.trace("leaving onUpdateCommandTag({}, {})", sourceCommandTag.getId(), oldSourceCommandTag.getId());
  }

  private Selector getJapcSelector(ISourceDataTag tag) {
    Selector selector = Selectors.NO_SELECTOR;
    JAPCHardwareAddress addr = (JAPCHardwareAddress) tag.getHardwareAddress();
    // if filter is defined
    if (addr.hasFilter()) {
      // split the filter ( expected format: key=value )
      String[] filter = addr.getFilter().split("=");
      Map<String, SimpleParameterValue> df = new HashMap<>();
      df.put(filter[0].trim(), SimpleParameterValueFactory.newSimpleParameterValue(filter[1].trim()));
      ParameterValue dataFilter = MapParameterValueFactory.newMapParameterValue(df);
      selector = SelectorFactory.newSelector(dataFilter);
    }
    
    if (selector.equals(Selectors.NO_SELECTOR) && addr.getCycleSelector() != null) {
      selector = SelectorFactory.newSelector(addr.getCycleSelector());
    }

    return selector;
  }

  @Override
  public void refreshAllDataTags() {
    // TODO Auto-generated method stub

  }

  @Override
  public void refreshDataTag(long dataTagId) {
    // TODO Auto-generated method stub

  }

  private static <T> T getValueOrDefault(T value, T defaultValue) {
    return value == null ? defaultValue : value;
  }
}
