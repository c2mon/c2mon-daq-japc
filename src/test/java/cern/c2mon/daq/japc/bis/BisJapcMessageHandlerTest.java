/******************************************************************************
 * Copyright (C) 2010-2021 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.daq.japc.bis;

import static cern.japc.ext.mockito.JapcMock.mockParameter;
import static cern.japc.ext.mockito.JapcMock.mpv;
import static cern.japc.ext.mockito.JapcMock.setAnswer;
import static org.awaitility.Awaitility.await;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import cern.japc.core.Parameter;
import cern.japc.core.Selectors;
import cern.japc.ext.mockito.answers.DefaultParameterAnswer;
import cern.rbac.util.authentication.LoginService;

import cern.c2mon.daq.japc.AbstractGenericJapcMessageHandlerTest;
import cern.c2mon.daq.japc.GenericJapcMessageHandler;
import cern.c2mon.daq.test.SourceDataTagValueCapture;
import cern.c2mon.daq.test.UseConf;
import cern.c2mon.daq.test.UseHandler;
import cern.c2mon.shared.common.datatag.util.SourceDataTagQualityCode;

@UseHandler(BisJapcMessageHandler.class)
public class BisJapcMessageHandlerTest extends AbstractGenericJapcMessageHandlerTest {

  @Before
  public void beforeClass() {
    GenericJapcMessageHandler.setLoginService(EasyMock.createMock(LoginService.class));
  }

  /**
   * This tests verifies the BisJapcMessageHandler's subscription mechanism.
   *
   * @throws Exception
   */
  @Test
  @UseConf("bis/e_japc_bis1.xml")
  public void subscription_Test1() throws Exception {

    messageSender.sendCommfaultTag(107211, "E_JAPC_JAPC1:COMM_FAULT", true, null);
    expectLastCall().once();

    SourceDataTagValueCapture sdtv = new SourceDataTagValueCapture();

    messageSender.addValue(EasyMock.capture(sdtv));

    expectLastCall().times(2);

    replay(messageSender);

    // Create Mock parameters

    Parameter p1 = mockParameter("CIBX.400.LN4.RF/BoardRegisters");

    String[] fields = { "registerNames", "registerValues" };

    // Array with names array

    String[] fieldNames1 = { "FIELD1", "STATUS", "FIELD2" };
    String[] fieldNames2 = { "FIELD1", "FIELD2", "STATUS" };

    float[] valueArray1 = { 0, 1, 2 };
    float[] valueArray2 = { 99, 0, 2 };

    Object[] values1 = { fieldNames1, valueArray1 };
    Object[] values2 = { fieldNames2, valueArray2 };

    setAnswer(p1, Selectors.NO_SELECTOR, new DefaultParameterAnswer(mpv(fields, values1)));

    await().atMost(1200, TimeUnit.MILLISECONDS).until(() -> {
      japcHandler.connectToDataSource();
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      // set the new value
      p1.setValue(Selectors.NO_SELECTOR, mpv(fields, values1));
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      // set the new value
      p1.setValue(Selectors.NO_SELECTOR, mpv(fields, values2));
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      verify(messageSender);
      return true;
    });

    assertEquals(SourceDataTagQualityCode.OK, sdtv.getFirstValue(54675L).getQuality().getQualityCode());
    assertEquals(1, sdtv.getFirstValue(54675L).getValue());

    assertEquals(SourceDataTagQualityCode.OK, sdtv.getLastValue(54675L).getQuality().getQualityCode());
    assertEquals(2, sdtv.getLastValue(54675L).getValue());
  }

  @Test
  @UseConf("bis/e_japc_bis1.xml")
  public void subscription_Test2() throws Exception {

    messageSender.sendCommfaultTag(107211, "E_JAPC_JAPC1:COMM_FAULT", true, null);
    expectLastCall().once();

    SourceDataTagValueCapture sdtv = new SourceDataTagValueCapture();

    messageSender.addValue(EasyMock.capture(sdtv));

    expectLastCall().times(1);

    replay(messageSender);

    // Create Mock parameters

    Parameter p1 = mockParameter("CIBX.400.LN4.RF/BoardRegisters");

    String[] fields = { "wrongFieldNames", "registerValues" };

    // Array with names array

    String[] fieldNames1 = { "FIELD1", "STATUS", "FIELD2" };
    String[] fieldNames2 = { "FIELD1", "FIELD2", "STATUS" };

    float[] valueArray1 = { 0, 1, 2 };
    float[] valueArray2 = { 99, 0, 2 };

    Object[] values1 = { fieldNames1, valueArray1 };
    Object[] values2 = { fieldNames2, valueArray2 };

    setAnswer(p1, Selectors.NO_SELECTOR, new DefaultParameterAnswer(mpv(fields, values1)));

    await().atMost(1200, TimeUnit.MILLISECONDS).until(() -> {
      japcHandler.connectToDataSource();
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      // set the new value
      p1.setValue(Selectors.NO_SELECTOR, mpv(fields, values1));
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      // set the new value
      p1.setValue(Selectors.NO_SELECTOR, mpv(fields, values2));
      return true;
    });

    await().atMost(1000, TimeUnit.MILLISECONDS).until(() -> {
      verify(messageSender);
      return true;
    });

    assertEquals(SourceDataTagQualityCode.INCORRECT_NATIVE_ADDRESS,
        sdtv.getFirstValue(54675L).getQuality().getQualityCode());
    assertEquals("field: registerNames not found", sdtv.getFirstValue(54675L).getQuality().getDescription());
  }

}
