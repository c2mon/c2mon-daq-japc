package cern.c2mon.daq.japc;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cern.c2mon.shared.common.datatag.SourceDataTag;

public class TagConnectionMonitorTest {
  private TagConnectionMonitor tagConnectionMonitor;
  private ITagRegister tagRegister;
  
  @Before
  public void before() {
    tagRegister = EasyMock.createMock(ITagRegister.class);
    tagConnectionMonitor = new TagConnectionMonitor(tagRegister);
  }
  
  @Test
  public void testAdd() {
    // init
    SourceDataTag tag = new SourceDataTag(1L, "testAdd", false);
    tagConnectionMonitor.add(tag);
    Assert.assertEquals(TagConnectionMonitor.MIN_RECONNECTION_TIME, tagConnectionMonitor.getReconnectInMap().get(tag.getId()));
    Assert.assertEquals(1, tagConnectionMonitor.getFutures().size());
  }
  
}
