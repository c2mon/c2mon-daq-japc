/******************************************************************************
 * Copyright (C) 2010-2016 CERN. All rights not expressly granted are reserved.
 *
 * This file is part of the CERN Control and Monitoring Platform 'C2MON'.
 * C2MON is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the license.
 *
 * C2MON is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with C2MON. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
package cern.c2mon.daq.japc;

import java.util.Calendar;

import org.easymock.Capture;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import cern.japc.core.*;
import cern.japc.core.factory.MapParameterValueFactory;
import cern.japc.ext.mockito.JapcMock;
import cern.japc.ext.mockito.SuperCycle;
import cern.japc.value.MapParameterValue;
import cern.japc.value.SimpleParameterValue;

import cern.c2mon.daq.common.conf.core.ConfigurationController;
import cern.c2mon.daq.test.GenericMessageHandlerTest;
import cern.c2mon.daq.test.UseConf;
import cern.c2mon.daq.tools.equipmentexceptions.EqCommandTagException;
import cern.c2mon.shared.common.datatag.SourceDataTagValue;
import cern.c2mon.shared.daq.command.SourceCommandTagValue;

import static cern.japc.ext.mockito.JapcMatchers.anyParameterValue;
import static cern.japc.ext.mockito.JapcMatchers.anySelector;
import static cern.japc.ext.mockito.JapcMock.*;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;

/**
 * This class implements a common parent class for JUnit testing framework for JAPC EquipmentMessageHandlers.
 *
 * @author wbuczak
 */
public abstract class AbstractGenericJapcMessageHandlerTest extends GenericMessageHandlerTest {

    protected GenericJapcMessageHandler japcHandler;

    protected SuperCycle spsSupercycle;
    protected SuperCycle lhcSuperCycle;

    protected final long getStartMonitoringTimeout() throws Exception {
        // reduce the timeout to 100ms only for tests
        return 100;
    }

    static {
        JapcMock.mockAllServices();
    }

    // if set, JAPC mockito framework will be initialized
    static boolean initMockito = true;

    @Override
    protected void beforeTest() throws Exception {
        japcHandler = (GenericJapcMessageHandler) msgHandler;

        if (initMockito) {
            resetJapcMock();
        }

        // GenericJapcMessageHandler.MSTART_RETRY_TIMOUT = getStartMonitoringTimeout();

        configurationController = new ConfigurationController();

        configurationController.setProcessConfiguration(pconf);

        pconf.getEquipmentConfigurations().put(equipmentConfiguration.getId(), equipmentConfiguration);

        configurationController.putImplementationCommandTagChanger(equipmentConfiguration.getId(), japcHandler);
        configurationController.putImplementationDataTagChanger(equipmentConfiguration.getId(), japcHandler);

        // configurationController.putImplementationEquipmentConfigurationChanger(TEST_EQUIPMENT_ID,
        // equipmentConfigurationChanger);
    }

    @Override
    protected void afterTest() {
        if (initMockito) {
            stopMockSupercycles();
            JapcMock.resetJapcMock();
        }
    }

    @Override
    @After
    public void cleanUp() throws Exception {
        if (japcHandler != null)
            japcHandler.disconnectFromDataSource();

        if (initMockito) {
            stopMockSupercycles();
            JapcMock.resetJapcMock();
        }
    }

    // @Test
    @UseConf("e_japc_test1.xml")
    public void testStartMonitoringExceptionThrown() throws Exception {

        JapcMock.setSubscriptionAnswer(new JapcMock.SubscriptionAnswer() {

            @Override
            protected SubscriptionHandle createSubscription(Parameter mock, Selector selector,
                    ParameterValueListener parameterValueListener) {

                if ("D1/P1".equals(mock.getName())) {
                    SubscriptionHandle sh = createMock(SubscriptionHandle.class);
                    try {
                        sh.startMonitoring();
                        expectLastCall().andThrow(new ParameterException("Simulated1"));
                        sh.startMonitoring();
                        expectLastCall().andThrow(new ParameterException("Simulated2"));
                        sh.startMonitoring();
                        expectLastCall().once();

                        expect(sh.getParameter()).andReturn(mock);
                        sh.stopMonitoring();
                        replay(sh);
                    } catch (Throwable t) { // should never happen
                    }

                    return sh;

                } else {
                    return super.createSubscription(mock, selector, parameterValueListener);
                }
            }
        });

        messageSender.sendCommfaultTag(107211, "CommFault", true, null);
        expectLastCall().once();

        Capture<SourceDataTagValue> sdtv = EasyMock.newCapture();

        messageSender.addValue(and(EasyMock.capture(sdtv), isA(SourceDataTagValue.class)));

        expectLastCall().once();

        replay(messageSender);

        japcHandler.connectToDataSource();

        Thread.sleep(1000);

        verify(messageSender);
    }

    @Test()
    @UseConf("e_japc_test7.xml")
    public void commandExecutionTest1() throws Exception {

        if (!initMockito)
            return;

        Parameter p1 = mockParameter("D7/P7");
        Parameter p2 = mockParameter("D8/P8");
        Parameter p3 = mockParameter("D9/P9");

        Selector s1 = sel("SPS.USER.TESTCYCLE");

        // Register behavior for setValue(..)
        doThrow(pe("Server is down")).when(p3).setValue(anySelector(), anyParameterValue());

        try {
            SourceCommandTagValue sctv = new SourceCommandTagValue(100847L, "TEST:TESTCMD1", 5250L, (short) 0, 100,
                    "Integer");

            japcHandler.sendCommand(sctv);

            MapParameterValue mpv = MapParameterValueFactory.newMapParameterValue(new String[] { "field1" },
                    new SimpleParameterValue[] { spv(100) });

            org.mockito.Mockito.verify(p1).setValue(ArgumentMatchers.eq(s1), ArgumentMatchers.eq(mpv));

        } catch (EqCommandTagException ex) {
            fail("EqCommandTagException was NOT expected at this point. Exception message: " + ex.getErrorDescription());
        }

        try {
            SourceCommandTagValue sctv = new SourceCommandTagValue(100848L, "TEST:TESTCMD2", 5250L, (short) 0, 12.43f,
                    "Float");

            japcHandler.sendCommand(sctv);

            org.mockito.Mockito.verify(p2).setValue(anySelector(), ArgumentMatchers.eq(spv(12.43f)));

        } catch (EqCommandTagException ex) {
            fail("EqCommandTagException was NOT expected at this point. Exception message: " + ex.getErrorDescription());
        }

        try {
            SourceCommandTagValue sctv = new SourceCommandTagValue(100849L, "TEST:TESTCMD1", 5250L, (short) 0, 100,
                    "Integer");

            japcHandler.sendCommand(sctv);

            fail("EqCommandTagException was expected at this point!");

        } catch (EqCommandTagException ex) {

        }
    }

    @Test
    public void testConvertSourceTimestampToMs() {

        if (!initMockito)
            return;

        Calendar cal = Calendar.getInstance();
        cal.set(1970, 1, 15);
        assertEquals(cal.getTimeInMillis() * 1000,
                GenericJapcMessageHandler.convertSourceTimestampToMs(cal.getTimeInMillis()));

        cal.set(2010, 8, 17);

        assertEquals(cal.getTimeInMillis(), GenericJapcMessageHandler.convertSourceTimestampToMs(cal.getTimeInMillis()));
    }

    public void stopMockSupercycles() {

        if (spsSupercycle != null) {
            spsSupercycle.stop();
            spsSupercycle = null;
        }

        if (lhcSuperCycle != null) {
            lhcSuperCycle.stop();
            lhcSuperCycle = null;
        }
    }

}
