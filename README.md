# C2MON DAQ JAPC
[![build status](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/badges/master/build.svg)](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/commits/master)

The JAPC DAQ allows acquiring data for C2MON from RDA3 publishers.

To learn more about DAQ process and Equipment, please read the following guideline:
http://c2mon.web.cern.ch/c2mon/docs/latest/core-concepts/process-equipment/

## Installation
Install the JAPC (RDA) DAQ either with the [distribution tarball](http://artifacts.cern.ch/list/beco-release-local/cern/c2mon/daq/c2mon-daq-japc/) that is generated with every release. Alternatively, you can launch the [Docker image](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/container_registry)


# DAQ Configuration 

## Equipment handler

The default Equipment handler to use is the following:
- `EquipmentMessageHandler` class: [`cern.c2mon.daq.japc.GenericJapcMessageHandler`](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/blob/master/src/main/java/cern/c2mon/daq/japc/GenericJapcMessageHandler.java)

Please note, that there is a set of dedicated JAPC handlers inheriting from the generic one, but for most of the cases the standard `EquipmentMessageHandler` is enough:
- [`BisJapcMessageHandler`](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/blob/master/src/main/java/cern/c2mon/daq/japc/bis/BisJapcMessageHandler.java): A dedicated JAPC handler for BIS
- [`GmJapcMessageHandler`](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/blob/master/src/main/java/cern/c2mon/daq/japc/gm/GmJapcMessageHandler.java): A dedicated JAPC handler for GM devices
- [`RdaJapcMessageHandler`](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/blob/master/src/main/java/cern/c2mon/daq/japc/rda/RdaJapcMessageHandler.java): Same like `GenericJapcMessageHandler`, but with RBA authentication support
- [`WieJapcMessageHandler`](https://gitlab.cern.ch/c2mon/c2mon-daq-japc/blob/master/src/main/java/cern/c2mon/daq/japc/wie/WieJapcMessageHandler.java): A dedicated JAPC handler for Wiener Fan Traydevices


### Equipment configuration example
The following code example shows how to create a DAQ process with one `GenericJapcMessageHandler` equiment, which can be used to subscribe to RDA publications:

```java
ConfigurationService configurationService = C2monServiceGateway.getConfigurationService();

configurationService.createProcess("P_JAPC");
configurationService.createEquipment("P_JAPC", "E_GenericJapcHandler", "cern.c2mon.daq.japc.GenericJapcMessageHandler");
```

To learn more about how to configure in C2MON a new DAQ Equipment please read the following guideline:
http://c2mon.web.cern.ch/c2mon/docs/latest/user-guide/client-api/configuration/#configuration-api



## Tag Hardware Address

- `DataTagHardwareAddress` class: [cern.c2mon.shared.common.datatag.address.impl.JAPCHardwareAddress](https://gitlab.cern.ch/c2mon/c2mon/blob/master/c2mon-shared/c2mon-shared-common/src/main/java/cern/c2mon/shared/common/datatag/address/impl/JAPCHardwareAddressImpl.java)- 

The follwowing parameters can be set:

| Parameter     | Type   | Mandatory? | Description                                                                                                                                                                                  |
|---------------|--------|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| deviceName    | String | Yes        | The name of the device                                                                                                                                                                       |
| propertyName  | String | Yes        | The name of the property within the device                                                                                                                                                   |
| dataFieldName | String | No         | Specifies the name of a field (MAP publications only)                                                                                                                                        |
| columnIndex   | int    | No         | Defines element's index inside the linear or 2d array. Unless used, set to -1 by default                                                                                                     |
| cycleSelector | String | No         | The cycle descriptor describing how often the value shall be acquired by,JAPC. If null, the value will be acquired on change. However, the `onChange` model is not supported by all devices. |


### Tag configuration example

The following code example shows how to add a new `Integer` type Tag with name `my-new-rda-tag` to an existing existing Equipment `E_GenericJapcHandler`:

```java
DataTagAddress tagAddress = new DataTagAddress(new JAPCHardwareAddressImpl("deviceFoo", "propertyFoo"));
DataTag tagToCreate = DataTag.create("my-new-rda-tag", Integer.class, tagAddress).build();

// send tag create configuration to C2MON
configurationService.createDataTag("E_GenericJapcHandler", tagToCreate);
```

To learn more about how to configure a C2MON `DataTag` with a specific tag address, please read the following guideline:
http://c2mon.web.cern.ch/c2mon/docs/latest/user-guide/client-api/configuration/#configuring-datatags
